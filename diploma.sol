pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract DiplomaAssign{
    
    struct ogrenci{
        string name; // name of student
        uint16 studentNumber;
        string schoolName; // School that graduated
        uint dateofEnroll;
        uint dateofGraduate;
        string GPA;
        bytes32 a;
        uint SchoolId;
    }
    
    struct signer{
        string name; // name of signer
        bytes32 privateKey;
        uint signerId;
    }
    
    mapping(bytes32 => ogrenci) ogrenciler;
    
    function _Diploma (string name, uint16 studentNumber, string schoolName, uint dateofEnroll, uint dateofGraduate, string GPA, uint schoolId ) public returns(ogrenci) {
        
        ogrenci memory o;
        o.name = name;
        o.studentNumber = studentNumber;
        o.schoolName = schoolName;
        o.dateofEnroll = dateofEnroll;
        o.dateofGraduate = dateofGraduate;
        o.GPA = GPA;
        o.SchoolId = schoolId;
        
        Hashing(o);
        
        return o;
    }
    
    function Hashing(uint signerSid, ogrenci o ) public returns (bytes32) {
    
    signer memory s;
    
    
    s.signerId = signerSid;
        
        
        if(o.SchoolId == signerSid){
        bytes32 a;

       a = keccak256(abi.encodePacked(o.name, o.studentNumber, o.GPA));
        
        
        ogrenciler[a] = o;
        
       // return o;
        return a;
        }
    }
    
    
}
